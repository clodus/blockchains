Blockchain, Ethereum, DApp, Solidity, Truffle, Ganache

Application décentralisée sur ethereum

DApp : ensemble de smart contract qui communique entre eux pour former une application

Installer Ganache : https://www.trufflesuite.com/docs/ganache/quickstart


EVM : Ethereum, Virtual, Machine
IDE : VS Code avec extension Solidity
Truffle : pour créer des smart contracts
Ganache : simulateur de réseau Ethereum

sudo npm install -g truffle

```
set HOME_BACKUP=$HOME
export HOME=/tmp/truffle_install
mkdir -p $HOME/.config/truffle
chmod a+rwx $HOME/.config               
chmod a+rwx $HOME/.config/truffle     # <- install fails with EACCESS (dir doesn't exists)
npm install -g truffle@${TRUFFLE_VERSION} # <- $HOME is ignored. Install in correct path
                                         # /opt/nvm/versions/node/v10.16.3/bin/truffle in my setup
rm -rf $HOME   # <- I can remove it once installed. Weird!!!???
set HOME=$HOME_BACKUP
```


truffle init

Lancer Ganache : chmod a+x Ganache.appImage

Créer premier contract : 

truffle create contract RobotGame